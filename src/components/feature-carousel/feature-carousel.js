import React, { Component } from 'react';
import CarouselItem from './carousel-item';
import Slider from 'react-slick';
import './feature.scss';

class FeatureCarousel extends Component {
  constructor() {
    super();
    this.showNextSlide = this.showNextSlide.bind(this);
    this.showPrevSlide = this.showPrevSlide.bind(this);
  }

  showNextSlide() {
    this.slider.slickNext();
  }

  showPrevSlide() {
    this.slider.slickPrev();
  }

  componentWillMount() {
    this.settings = {
      dots: false,
      arrows: false,
      autoplay: false,
      infinite: true,
      speed: 300,
      nextArrow: this.nextArrow,
      prevArrow: this.prevArrow,
      slidesToShow: 1,
      slidesToScroll: 1,
      fade: false
    };

    this.slidesToShow = Object
                      .keys(this.props.forSale)
                      .map(key => <div key={key}><CarouselItem key={key} details={this.props.forSale[key]} /></div>)
    this.slider = <Slider ref={c => this.slider = c } {...this.settings}>{this.slidesToShow}</Slider>
  }

  render() {
    const featuresHomes = this.slider
    return (
       <div className="row">
          <div className="container">
            <h1>Featured Properties</h1>
            <div className="propertyCarousel">
              {
                featuresHomes
              }
              <div ref={div => this.nextArrow = div } onClick={this.showNextSlide} className="slick-next icon nextArrow"></div>
              <div ref={div => this.prevArrow = div } onClick={this.showPrevSlide} className="slick-prev icon prevArrow"></div>
            </div>
          </div>
        </div>
    )
  }
}

export default FeatureCarousel;