import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './footer.scss';

class Footer extends Component {
  constructor() {
    super();
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e, route) {
    e.preventDefault();
    this.context.router.history.push(route);
  }

  render() {
    return (
      <footer className="footer" role="contentinfo">
          <div className="container">
            <div className="row">
              <div className="col-xs-12 col-sm-4">
                <h1>Navigation</h1>
                <ul className="footer-nav">
                  <li><a href="#" onClick={(e) => this.handleClick(e, '/')}>Home</a></li>
                  <li><a href="#" onClick={(e) => this.handleClick(e, '/sale')}>For Sale</a></li>
                  <li><a href="#" onClick={(e) => this.handleClick(e, '/rent')}>To Rent</a></li>
                  <li><a href="#" onClick={(e) => this.handleClick(e, '/calculator')}>Calculator</a></li>
                  <li><a href="#" onClick={(e) => this.handleClick(e, '/company-profile')}>Our Company</a></li>
                  <li><a href="#" onClick={(e) => this.handleClick(e, '/contact')}>Contact</a></li>
                </ul>
              </div>
              <div className="col-xs-12 offset-sm-4 col-sm-4">
                <h1>Home Specialist Premier Property</h1>
                <p>Physical Address</p>
                <p>27 Trisula Avenue<br />Arena Park<br />Chatsworth</p>
                <p>Contact Details</p>
                <p><strong>Tel:</strong>031 404 2564
                <br /><strong>Cell:</strong>076 256 6711
                <br /><strong>Fax:</strong>086 263 1247
                <br /><strong>Email: </strong><a href="mailto:homespecialistinfo@gmail.com">homespecialistinfo@gmail.com</a></p>
              </div>
            </div>
          </div>
        </footer>
    );
  }
}

export default Footer;

Footer.contextTypes = {
  router: PropTypes.object
}
