import React from 'react';
import './featured-links.scss';

class FeaturedLinks extends React.Component {
  render() {
      return (
        <div id='featured-links'>
          <div className="container">
            <div className="row">
              <div className="col-xs-12 col-sm-4">
                <div className="featured-content">
                  <i className="icon icon-about"></i>
                  <h2>About us</h2>
                  <p>Your property experts in real estate.<br />
                  We specialise in Buying, Selling, Rentals, Renovation, Valuations</p>
                </div>
              </div>
              <div className="col-xs-12 col-sm-4">
                <div className="featured-content">
                  <i className="icon icon-listing"></i>
                  <h2>List your property</h2>
                  <p>List with us and experience a hassle free property sale</p>
                </div>
              </div>
              <div className="col-xs-12 col-sm-4">
                <div className="featured-content">
                  <i className="icon icon-contact"></i>
                  <h2>Contact Us</h2>
                  <p>Contact us today for an obligation free valuation on your property</p>
                  </div>
              </div>
            </div>
          </div>
        </div>
      )
  }
}

export default FeaturedLinks;
